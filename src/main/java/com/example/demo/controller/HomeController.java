package com.example.demo.controller;

import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/app")
public class HomeController extends SpringBootServletInitializer {

    @GetMapping(value = "/get/cityName")
    public String getCityName() {
        System.out.println("City is ahmedabad");
        return "ahmedabad goa";
    }
}
